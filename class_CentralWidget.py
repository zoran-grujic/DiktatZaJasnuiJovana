from PyQt5 import QtCore #conda install pyqt
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QGridLayout, QWidget, QTableWidget, QTableWidgetItem, QSizePolicy
from PyQt5.QtCore import QSize, Qt, pyqtSignal, pyqtSlot
from PyQt5 import QtGui
from pickle import FALSE
from PyQt5.Qt import QEvent

import os


class CentralWidget(QtWidgets.QWidget):
    
    delayLine = 2
    delayWord = 1
    talkFast = True
    
    def __init__(self, parent): 
        super().__init__()
        self.parent = parent
        
        self.selectFileButton = QtWidgets.QPushButton('Open lesson file')
        self.selectFileButton.clicked.connect(self.parent.openFile)
        
        self.talkSpeedLabel = QtWidgets.QLabel("Talk speed?")
        self.delayLineLabel = QtWidgets.QLabel("Line delay: ")
        self.delayWordLabel = QtWidgets.QLabel("Word delay: ")
        self.autoPlayLabel = QtWidgets.QLabel("Auto play?")
        
        mode_group=QtWidgets.QButtonGroup(self) # Number group
        self.taklFastRadio =QtWidgets.QRadioButton("Fast")
        self.taklFastRadio.name = "Fast"        
        toolTipMSG = '<span style="color:#555555;">Talk fast</span>' 
        self.taklFastRadio.setToolTip(toolTipMSG)
        mode_group.addButton(self.taklFastRadio)
        self.taklSlowRadio =QtWidgets.QRadioButton("Slow")
        self.taklSlowRadio.name = "Slow"        
        toolTipMSG = '<span style="color:#555555;">Talk slow</span>' 
        self.taklSlowRadio.setToolTip(toolTipMSG)
        mode_group.addButton(self.taklSlowRadio)
        
        if self.talkFast:
            self.taklFastRadio.setChecked(True)
        else:
            self.taklSlowRadio.setChecked(True)
        mode_group.buttonToggled.connect(self.onChange)
        
        self.delayLineQLE = QtWidgets.QLineEdit()
        self.delayLineQLE.name = "delayLine"
        regexpReal = QtCore.QRegExp('^[+]?([.]\d+|\d+[.]?\d*)$')
        #self.plotPointsEdit.setValidator(QtGui.QIntValidator())#enter only integers?       
        self.delayLineQLE.setValidator( QtGui.QRegExpValidator(regexpReal) )#accept real numbers
        toolTipMSG = '<span style="color:#555555;">Seconds between lines</span>' 
        self.delayLineQLE.setToolTip(toolTipMSG)
        self.delayLineQLE.setMaxLength(15)
        self.delayLineQLE.setAlignment(Qt.AlignRight)
        self.delayLineQLE.setMaximumWidth(100)
        self.delayLineQLE.setText(str(self.delayLine))
        self.delayLineQLE.textChanged.connect(self.onChange)
        
        self.delayWordQLE = QtWidgets.QLineEdit()
        self.delayWordQLE.name = "delayWord"
        regexpReal = QtCore.QRegExp('^[+]?([.]\d+|\d+[.]?\d*)$')
        #self.plotPointsEdit.setValidator(QtGui.QIntValidator())#enter only integers?       
        self.delayWordQLE.setValidator( QtGui.QRegExpValidator(regexpReal) )#accept real numbers
        toolTipMSG = '<span style="color:#555555;">Seconds between lines</span>' 
        self.delayWordQLE.setToolTip(toolTipMSG)
        self.delayWordQLE.setMaxLength(15)
        self.delayWordQLE.setAlignment(Qt.AlignRight)
        self.delayWordQLE.setMaximumWidth(100)
        self.delayWordQLE.setText(str(self.delayWord))
        self.delayWordQLE.textChanged.connect(self.onChange)
        
        self.autoPlayQChk = QtWidgets.QCheckBox()
        toolTipMSG = '<span style="color:#555555;">If checked play continuously</span>' 
        self.autoPlayQChk.setToolTip(toolTipMSG)
        self.autoPlayQChk.setChecked(False)
        self.autoPlayQChk.clicked.connect(self.autoPlay)
        
        topVBox =QtWidgets.QVBoxLayout()
        
        formHBox = QtWidgets.QHBoxLayout()
        
        formGridLayout = QtWidgets.QGridLayout()
        #for i in range(5):
        #    formGridLayout.setColumnStretch(i, 1)
        row=0         
        formGridLayout.addWidget(self.talkSpeedLabel,row,1,Qt.AlignRight)#row, col, rowspan, colspan
        formGridLayout.addWidget(self.taklFastRadio,row,2,Qt.AlignLeft)
        formGridLayout.addWidget(self.taklSlowRadio,row,3,Qt.AlignLeft)
        
        row += 1      
        formGridLayout.addWidget(self.delayLineLabel,row,1,Qt.AlignRight)#row, col, rowspan, colspan
        formGridLayout.addWidget(self.delayLineQLE,row,2,Qt.AlignLeft)
        #formGridLayout.addWidget(self.scanSymmetricLabelDC,row,3,Qt.AlignCenter)
        row += 1
        formGridLayout.addWidget(self.delayWordLabel,row,1,Qt.AlignRight)#row, col, rowspan, colspan
        formGridLayout.addWidget(self.delayWordQLE,row,2,Qt.AlignLeft)
        
        row += 1
        formGridLayout.addWidget(self.autoPlayLabel,row,1,Qt.AlignRight)#row, col, rowspan, colspan
        formGridLayout.addWidget(self.autoPlayQChk,row,2,Qt.AlignLeft)
        
        formHBox.addStretch(1)
        formHBox.addLayout(formGridLayout)
        formHBox.addStretch(1)
        
        topVBox.addLayout(formHBox)
        topVBox.addStretch(1)
        topVBox.addWidget(self.selectFileButton)
        topVBox.addStretch(1)
        
        self.setLayout(topVBox)
        
    def autoPlay(self):
        
        if self.autoPlayQChk.isChecked():
            try:
                
                self.delayLine = float(self.delayLineQLE.text())
                self.delayWord = float(self.delayWordQLE.text())
                if self.taklFastRadio.isChecked():
                    self.talkFast = True
                else:
                    self.talkFast = False
                
                self.parent.delayPlay()
            except Exception as e:
                print("", e)
        else:
            self.parent.delayTimer.stop()
    
    def onChange(self):
        
        try:
            self.delayLine = float(self.delayLineQLE.text())
        except:
            pass
        try:
            self.delayWord = float(self.delayWordQLE.text())
        except:
            pass
        if self.taklFastRadio.isChecked():
            self.talkFast = True
        else:
            self.talkFast = False
if __name__ == '__main__':
    os.system("python SpeakApp.pyw")