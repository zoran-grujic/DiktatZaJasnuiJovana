Colin can't climb out of the giant web.
Then he looks down...

Oh no! Spiders!
Giant spiders! I don't like spiders!

But Clara has got a rope!

Colin - catch this!
And climb up!

A spider sees Colin and starts to climb up too.
Quick, Colin. Don't look down!
You can do it!

Now run!
Colin and Clara safe but then...

Stop! Please help us!
The spiders have got my sister!
And my brother! 

Can Colin and Clara help?
Where are they?
Over there in the Black Wood.
OK, Colin let's go!

Colin and Clara are in the wood but Clara doesn't like it!
Look all the webs! 
They're everywhere!
But where are the spiders?
And where are the children?

Then Colin sees some large apples on a tree.
An apple!
Great! I'm hungry.
What? No, Colin! Stop!
Don't eat it!