People in Britain eat food from lots of different countries but here are some traditional meals.

This man always has breakfast in a cafe.
He has a traditional English breakfast - 
an egg, bacon, sausages, tomatoes, mushrooms, beans, toast and tea.

It's lunch time at this pub by the river.
This is a poloughmans's lunch.
There's bread, cheese, salad and chutney.

In this tea shop you can have scones with cream and strawberry jam.
This is called a cream tea.
People in Britain don't have this every day.
A cream tea is a special treat.

This fish and chips shop is open at lunch time and at the evening.
You can buy hot fish and chips here and then eat them at home.
