I asked for a tulip, 
I got a garden, 
I asked for a drop, 
I got the sea, 
I asked for love and you were sent to me!

Like a jewel, 
true love is rare to find, 
but I'm lucky because I found you!

You've changed my life for the better. 
I am inspired to be different when I'm with you!