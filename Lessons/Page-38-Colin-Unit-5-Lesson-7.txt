Can Colin, Clara and the children escape from the giant spiders?

Quick! Run faster!
I can't! I'm smaller than you!
It's OK.
Hold my hand!Oh no!
There are spiders everywhere!

What can we do?
Quick Colin - use your calculator!

OK, how many spiders are there?
There are six - no seven!

Colin puts the numbers into his calculator.

Seven-minus-seven...

And spiders disappear!
Seven minus seven is zero!
Yes, that's right!

They're safe! Hooray!
but where's Colin?

Colin is back home again!
It's upside down!