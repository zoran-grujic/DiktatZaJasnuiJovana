import sys
import os
import time
from gtts import gTTS
from playsound import playsound


from PyQt5 import QtCore #conda install pyqt
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QFileDialog, QMainWindow, QGridLayout, QWidget, QTableWidget, QTableWidgetItem, QSizePolicy
from PyQt5.QtCore import QSize, Qt
from PyQt5 import QtGui
from PyQt5.Qt import QEvent
import threading

import class_CentralWidget

class MainWindow(QtWidgets.QMainWindow):
    
    menu=None
    mainWidget=None
    status=None
    
    currentLine = 0
    
    maxN = 0
    
    def __init__(self):
        super().__init__()
        # You must call the super class method

        self.setWindowIcon(QtGui.QIcon('icons/audio-icon.png'))
        
        self.setMinimumSize(QtCore.QSize(1000, 600))         # Set sizes 
        self.setWindowTitle("Diktat za Jasnu i Jocu")    # Set the window title
        
        
        #-----------------------------------------------------
        #
        #                  Status bar - bottom of the window
        #
        #-----------------------------------------------------
        self.status = self.statusBar()
        self.status.showMessage('Ready')
        
        
        
        #-----------------------------------------------------
        #
        #                  Actions
        #
        #----------------------------------------------------- 
        #http://www.iconarchive.com 
        exitAct = QtWidgets.QAction(QtGui.QIcon('icons/power.png'), '&Exit', self)        #QIcon.fromTheme('document-new')
        exitAct.setShortcut('Ctrl+X')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(self.close)#QtWidgets.qApp.quit
        
        currentAct = QtWidgets.QAction(QtGui.QIcon('icons/play-icon.png'), '&Ponovi', self)        #QIcon.fromTheme('document-new')
        currentAct.setShortcut('p' )
        currentAct.setStatusTip('Pusti')
        currentAct.triggered.connect(self.current)
        
        nextAct = QtWidgets.QAction(QtGui.QIcon('icons/forward-icon.png'), '&Sledeća', self)        #QIcon.fromTheme('document-new')
        nextAct.setShortcut('Enter' )
        nextAct.setStatusTip('Sledeća')
        nextAct.triggered.connect(self.next)
        
        backAct = QtWidgets.QAction(QtGui.QIcon('icons/rewind-icon.png'), '&Nazad', self)        #QIcon.fromTheme('document-new')
        backAct.setShortcut('-')
        backAct.setStatusTip('Prethodna linija')
        backAct.triggered.connect(self.previous)#QtWidgets.qApp.quit
        
        """
        #-----------------------------------------------------
        #
        #                  Main menu
        #
        #-----------------------------------------------------
        #http://zetcode.com/gui/pyqt5/menustoolbars/
        self.menu = self.menuBar() 
        
        #-----------------------------------------------------
        #                  File menu
        #-----------------------------------------------------
        fileMenu = self.menu.addMenu('&File')       
        fileMenu.addAction(exitAct)
        
        #-----------------------------------------------------
        #                  DAQ menu
        #-----------------------------------------------------
        daqMenu = self.menu.addMenu('&DAQ')
        daqMenu.addAction(nextAct)
        daqMenu.addAction(backAct)
        
        """
        
        #-----------------------------------------------------
        #
        #                  The toolbar
        #
        #-----------------------------------------------------
        self.toolbar = self.addToolBar('Control')        
        
               
        self.toolbar.addAction(backAct)
        self.toolbar.addAction(currentAct)
        self.toolbar.addAction(nextAct)
        
        
        
        #-----------------------------------------------------
        #
        #                  Central widget
        #
        #-----------------------------------------------------
        self.centralWidgetO = class_CentralWidget.CentralWidget(self)#give reference to the parent
        self.setCentralWidget(self.centralWidgetO)
        
        
        #-----------------------------------------------------
        #
        #                  make mp3
        #
        #----------------------------------------------------
        self.makemp3()
        
        
        #-----------------------------------------------------
        #
        #                  Status bar - bottom of the window
        #
        #-----------------------------------------------------
        self.status = self.statusBar()
        
        
        
        #start the show!
        self.show()
        
        self.delayTimer = QtCore.QTimer(self)
        self.delayTimer.timeout.connect(self.next)
        
        self.statusTimer = QtCore.QTimer(self)
        self.statusTimer.setInterval(400)
        self.statusTimer.timeout.connect(self.setStatus)
        self.statusTimer.start()
        
    def setStatus(self):
        self.status.showMessage("%d/%d" % (self.currentLine+1, self.maxN))
        
    def delayPlay(self):
        
        if(self.centralWidgetO.autoPlayQChk.isChecked()):
            self.current()
            interv = self.centralWidgetO.delayLine + self.contentWords[self.currentLine] * self.centralWidgetO.delayWord
            self.delayTimer.setInterval(interv*1000)
            self.delayTimer.start()
        else:
            pass    
        
    def openFileNameDialog(self):    
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,"Open file to read the text from", "Lessons/","Text Files (*.txt)", options=options)
        if fileName:
            return fileName
        else:
            return None
        
    def openFile(self):
        
        file = self.openFileNameDialog()
        
        if file == None:
            return
        else:
            self.makemp3(file)
        
    def makemp3(self, file = "Lessons/text.txt"):
        
        self.textFile= file
        
        self.makeMP3_Thread = threading.Thread(target=self._makemp3)
        self.makeMP3_Thread.daemon = True
        self.makeMP3_Thread.start() 
        
    def _makemp3(self):
        
        with open(self.textFile) as f:
            content = f.readlines()
        mp3DirName = "mp3/"+os.path.basename(self.textFile)
        if not os.path.exists(mp3DirName):
            os.makedirs(mp3DirName)
        # you may also want to remove whitespace characters like `\n` at the end of each line
        self.content = []
        self.contentWords = []
        for x in content:
            l=x.strip()
            if not l == "":
                self.content.append(l)
        
        self.maxN = len(self.content) 
        
        for i in range( self.maxN ):    
            text = self.content[i]            
            if text=="":
                #time.sleep(3)
                continue
            
            self.contentWords.append( len(text.split(" ")))
            
            print(text)
        
            tts = gTTS(text=text, lang='en', slow=not self.centralWidgetO.talkFast)
            fn = "%s/%05d.mp3" %(mp3DirName, i)
            print(fn)
            tts.save(fn)
        
        self.currentLine = 0
        self.status.showMessage("%d/%d" % (self.currentLine, self.maxN))
        
        ##############################################
        #make VLC play list
        
        myDir = os.getcwd()
        
        str2Save = '<?xml version="1.0" encoding="UTF-8"?> \
<playlist xmlns="http://xspf.org/ns/0/" xmlns:vlc="http://www.videolan.org/vlc/playlist/ns/0/" version="1">\
    <title>%s</title> \
    <trackList>\n' % (self.textFile)

        i=0
        for line in self.content:
            if line == '':
                continue            
            
            str2Save +='<track>\
            <location>file:///%s/mp3/%s/%05d.mp3</location>\
            <duration>2016</duration>\
            <extension application="http://www.videolan.org/vlc/playlist/0">\
                <vlc:id>%d</vlc:id>\
            </extension>\
            </track>\n'    % (myDir, os.path.basename(self.textFile), i, 2*i)
            
            #pause
            pause = len(line.split(" ")) * self.centralWidgetO.delayWord + self.centralWidgetO.delayLine
            print("pause: ",pause)
            str2Save +='<track>\
                <location>vlc://pause:%f</location>\
                <duration>792</duration>\
                <extension application="http://www.videolan.org/vlc/playlist/0">\
                    <vlc:id>%d</vlc:id>\
                </extension>\
            </track>\n' % ( pause, 2*i + 1)
            
            i += 1
            
        str2Save += '<extension application="http://www.videolan.org/vlc/playlist/0">\n'
        for j in range(2*i):
            str2Save += '<vlc:item tid="%d"/>\n' % (j)
        str2Save += '</extension>\n </playlist>'
        
        
        vlcListFN = "%s/mp3/%s-VLC-PlayList.xspf" % (myDir, os.path.basename( self.textFile) )
    
        
        text_file = open(vlcListFN, "w")
        text_file.write(str2Save)
        text_file.close()
            
        
        
    
    def play(self, line):
        
        mp3DirName = "mp3/"+os.path.basename(self.textFile)
        fname = "%s/%05d.mp3" % (mp3DirName , line)
        print("Play: %s" % (fname))
        if os.path.isfile(fname):
            playsound(fname)
        self.status.showMessage("%d/%d" % (self.currentLine, self.maxN))
       
    def playBySystem(self, line):       
        
        fname = "mp3/%05d.mp3" % (line)
        print("Play: %s" % (fname))
        if os.path.isfile(fname):
            os.system("start %s" % (fname))
        self.status.showMessage("%d/%d" % (self.currentLine, self.maxN))
        
        
    
    def next(self):   
        
        self.currentLine += 1
        
        if self.currentLine >= self.maxN:
            self.currentLine = self.maxN - 1 
            
        self.play(self.currentLine)
        
        if self.currentLine == self.maxN-1:
            self.delayTimer.stop()
    
    def current(self):
            
        self.play(self.currentLine)    
        
    def previous(self):
        
        self.currentLine -=1
        if self.currentLine < 1:
            self.currentLine = 0
            
        self.play(self.currentLine)
        #self.currentLine +=1
        
    def keyPressEvent(self, e):
        """
        Actions do have single Shortcut, 
        to add more we need to check for keyPressEvent
        """
            
        if e.key in (Qt.Key_Space, Qt.Key_Return, Qt.Key_Enter):
            self.next()
        
        if e.key == Qt.Key_M:
            self.previous()
            
        if e.key == Qt.Key_P:
            self.current()
            
    def closeEvent(self, e):        
        
        e.accept() 
        #e.ignore()
        
    
        

      
        
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())
    exit()